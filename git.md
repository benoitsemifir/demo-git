# Recap git

## Initialiser un repo Git

Avec la commande `git init`.

Ceci va créer un dossier caché appelé .git qui contient le récap des modifications apportées au projet.

## Afficher le status du repo

`git status`

## Ajouter un fichier au prochain commit

`git add <nom du fichier>`

## Commit l'état du projet

`git commit -m "Description du travail"`

## Ajouter un repo distant (remote)

`git remote add <variable du remote> <url du remote>`

Par exemple `git remote add origin https://benoitsemifir@gitlab.com/benoitsemifir/demo-git.git`

## Push: Envoyer les commits sur le remote

`git push <remote> <branche à envoyer>`

Par exemple `git push origin main`.

## Pull: Récupérer les commits du remote

`git pull <remote>`

Par exemple: `git pull`
