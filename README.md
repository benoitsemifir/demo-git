# Découverte Git

## Les titres

# titre de niveau 1

## titre de niveau 2

### etc

... titre

###### titre de niveau 6

```md
# titre de niveau 1
## titre de niveau 2
### etc...
...
###### titre de niveau 6
```

```py
print("Poulet")
```

## Les commandes

Avec les magic quotes (alt-gr + è)

Lancer un programme en Python: `py script.py`

```md
Lancer un programme en Python: `py script.py`
```

## Les notes

> Ceci est une note, elle attire l'attention sur quelque chose à prendre en compte comme une option.

```md
> Ceci est une note, elle attire l'attention sur quelque chose a prendre en compte comme une option.
```

## Les liens (ou ancres)

Permet la navigation (interne ou externe).

Voir [le site de Python.](https://www.python.org/)

```md
Voir [le site de Python.](https://www.python.org/)
```

Lien vers le [support de git](./git-base.pdf).

## Les images

Comme pour les ancres mais avec un point d'exclamation devant.

![Image de bord de mer](https://images.unsplash.com/photo-1624220684521-b4de9fbff4a7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80)

![texture](./texture.jpg)

```md
![](https://images.unsplash.com/photo-1624220684521-b4de9fbff4a7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80)
```

## Les tableaux

| id | nom | prenom |
|----|-----|--------|
|1|Dupond|Jean|
|2|Dupont|Jean|

## Les éléments de texte

### Le gras ou emphase

Pour mettre des éléments en **valeur**

```md
Pour mettre des éléments en **valeur**
```

### L'italique

Pour attirer l'attention sur un *paramètre* particulier.

```md
Pour attirer l'attention sur un *paramètre* particulier.
```

## Les listes

### Liste non ordonnées

Pratique pour l'énumération

- Faire les courses
- trouver une place pour se stationner
- être à l'heure
  - si possible avec les croissants

### Les listes ordonnées

Pratique pour ordonner des éléments.

Il est possible de faire des sous-listes. Il faut donc repartir à 1 dans la numérotation.

1) France
2) Belgique
3) Pays-bas
2) Allemagne
    1) Bayern de Munich
    1) Dortmund

```md
1) France
2) Belgique
3) Pays-bas
2) Allemagne
    1) Bayern de Munich
    1) Dortmund
```
