# Les branches

Les branches permettent de travailler sur une partie du projet sans impacter le travail des autres branches.

On peut voir les branches comme des lignes temporelles parallèles.

## Voir les branches

`git branch` affiche la liste des branches et celle sur laquelle nous nous trouvons.

```bash
$ git branch
* feature/branche
  main
```

## Créer une branche

Créer un branche **locale**. Il faut la pousser pour l'avoir en remote (`git push --all`).
`git branch nouvelle/branche`


Astuce: pour créer une branche et se placer directement dessus:
`git checkout -b nouvelle/branche`

## Se déplacer sur une branche

`git checkout nouvelle/branche`
(ça permet aussi de se déplacer sur des commits, détacher sa tête)

## Renomer une branche

`git branch -m nouveau-nom`

## Supprimer une branche

`git branch -d branche-a-supprimer`
